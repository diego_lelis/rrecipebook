import {NgModule} from "@angular/core";
import { ShoppingListComponent } from './shopping-list.component';
import { ShoppingListAddComponent } from './shopping-list-add/shopping-list-add.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared-module';
import {shoppingListRouting} from './shopping-list.routing';

@NgModule({
  declarations: [ShoppingListComponent, ShoppingListAddComponent],
  imports: [SharedModule, FormsModule, shoppingListRouting]
})
export class ShoppingListModule {}
