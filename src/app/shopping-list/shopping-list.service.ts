import {Ingredient} from "../ingredient";

export class ShoppingListService {
  private items: Ingredient[] = [];

  constructor() { }

  getItems() {
    return this.items;
  }

  addItems(items: Ingredient[]) {
    // Array.prototype.push.apply(this.items, items);
    // this.items.push(items);
    for (var item of items) {
      this.addItem(item);
    }
  }
  addItem(item: Ingredient) {
    let index = this.items.findIndex(auxItem => auxItem.name === item.name);
    if (index !== -1) {
      this.items[index].amount = +item.amount + +this.items[index].amount;
    } else {
      let newItem: Ingredient = new Ingredient(item.name, item.amount);
      this.items.push(newItem);
    }
  }

  editItem(oldItem: Ingredient, newItem: Ingredient) {
    this.items[this.items.indexOf(oldItem)] = newItem;
  }

  deleteItem(item: Ingredient) {
    this.items.splice(this.items.indexOf(item), 1);
  }
}
