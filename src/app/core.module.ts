import {NgModule} from '@angular/core';
import {DropdownDirective} from './dropdown.directive';
import {HomeComponent} from './home/home.component';

@NgModule ({
  declarations: [DropdownDirective, HomeComponent],
  exports: [DropdownDirective, HomeComponent]
})

export class CoreModule {}
